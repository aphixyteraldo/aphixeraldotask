<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Eraldo - Aphix - YoutubeAPI</title>
		<meta name="description" content="Take a look at this youtube API connection and find your favorites videos.">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		
		<!-- Fonts -->
		<link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

		<!-- Bootstrap CSS and JS-->
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>

		<!-- jQuery -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

		<!-- CSS -->
		<style type="text/css">
			.logoyt{
				width: 40px;
				height: 40px;
			}
			.span-title{
				margin-left: 10px;
			}
			#searchContent{
				border-radius: .25rem;
			}
			#btn-search{
				margin-left: 5px;
			}
			.header{
				border-top-style: solid;
				border-color: red;
				border-bottom: 10px;
			}
			.style-cont{
				margin-top:10px;
			}
			.resultVideosStyle{
				border-color: #f5c6cb;
				margin-bottom: 20px;
				border: 1px solid #f5c6cb;
				position: relative;
				padding: .75rem 1.25rem;
				border-radius: .25rem;
				box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
			}
			.imgResultVideos {
				border-radius: .25rem;
    			width: 100%;
			}
			.spansResultVideos{
				display: block;
			}
			.footer{
				border-bottom-style: solid;
    			border-color: red;
    			border-bottom: 10px;
			}
		</style>

	</head>
    <body>
		@include('header')
		<div class="container">
  			<div class="row justify-content-md-center">
			  	<div class="col-sm-6 col-md-6 col-lg-6">
				  	<form id="searchYt">
						<div class="input-group mb-3">
							<input id="searchContent" type="text" class="form-control" placeholder="Your Search" aria-label="Your Search" aria-describedby="basic-addon2">
							<div class="input-group-append">
								<button id="btn-search" class="btn btn-outline-danger" type="submit">Search</button>
							</div>
						</div>
					</form>
			  	</div>
			</div>
		</div>
		<div class="container">
			<div class="row justify-content-md-center">
				<div id="result-msg" class="col-sm-6 col-md-6 col-lg-6">
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row justify-content-md-center">
				<div id="result-videos" class="col-sm-6 col-md-6 col-lg-6">
				</div>
			</div>
		</div>
		@include('footer')
    </body>
	<script type="text/javascript">

		$("#searchYt").submit(function(e){
			$('#resultBoxAllert').remove();
			$('#allvideosdiv').remove();
			var search = $('#searchContent').val();
					
			$.ajax({
				method: "POST",
				url: "/youtubeFunctions",
				data: { "content": search, "option": "search" ,"_token": "{{ csrf_token() }}"}
			}).done(function( resultReturn ) {
				var result = JSON.parse(resultReturn);

				//no results = 1
				if(result['returnCode'] == 0){
					var htmlAlert = '<div id="resultBoxAllert" class="alert alert-danger" role="alert">'+
										'Results: <b>0</b> - Sorry, change your search and try again!'+
									'</div>';
					$('#result-msg').append(htmlAlert);
				}else{
					var htmlAlert = '<div id="resultBoxAllert" class="alert alert-success" role="alert">'+
										'Results: <b>' +result['returnTotalFound']+'</b> videos founded!'+
									'</div>';
					$('#result-msg').append(htmlAlert);
					var htmlVideos = '<div id="allvideosdiv">';
						
					$( result['videos'] ).each(function( index, element ) {
		
						htmlVideos += '<div class="row">'+
											'<div id="result-'+index+'" class="col-sm-12 col-md-12 col-lg-12">'+
												'<div class="resultVideosStyle">'+
													'<div class="row">'+
														'<div class="col-sm-6 col-md-6 col-lg-6">'+
															'<img class="imgResultVideos" src="'+element['photo']+'" alt="">'+
														'</div>'+
														'<div class="col-sm-6 col-md-6 col-lg-6">'+
															'<span class="spansResultVideos">'+
																'Channel: '+element['channelTitle']+' '+
															'</span>'+
															'<span class="spansResultVideos">'+
																'Title: '+element['videosTitle']+' '+
															'</span>'+
															'<span class="spansResultVideos">'+
																'Date: '+element['date'].replace('T', ' ').replace('Z', '')+' '+
															'</span>'+
															'<span class="spansResultVideos">'+
																'Link: <a target="_blank" rel="noopener noreferrer" href="'+element['videosLinks']+'">'+element['videosLinks']+'</a> '+
															'</span>'+
														'</div>'+
													'</div>'+
												'</div>'+
											'</div>'+
										'</div>';
					});
					htmlVideos += '</div>';
					$('#result-videos').append(htmlVideos);
				}
			});
			e.preventDefault();
		});

	</script>
</html>