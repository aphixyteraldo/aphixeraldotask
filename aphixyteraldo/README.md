!IMPORTANT!

PHP Version: 
            PHP 7.3.23 (cli) (built: Sep 29 2020 11:16:48) ( ZTS MSVC15 (Visual C++ 2017) x64 ) Copyright (c) 1997-2018 The PHP Group Zend Engine v3.3.23, Copyright (c) 1998-2018 Zend Technologies.

Laravel Version:
            Laravel Installer 4.0.5

Composer Version:
            Composer version 1.10.15 2020-10-13 15:59:09

HTTP request:
            Using GuzzleHttp native in Laravel.

---------------------------------------------------------------------------------------------------------------

*Limited to only 25 videos per search (parameter: maxResults on Controller).*

*Take care to not pass over the youtube API quota (10000 per day).*

*Search maximum results: 1000000 (defeined by youtube).*

*I decided to not use node.js to compile the CSS file because of the size of this tool so, I set pure CSS code in the HTML page*

Aphix Eraldo Almeida Vieira - task.

About: 

Build an MVC application using PHP and a popular framework which will use the YouTube API on the server side to return a list of YouTube search results by an ajax call.
Some coding restrictions:

    1.	The code should be created using any popular PHP framework such as Laminas, Laravel or Symfony and follow their best practices for code styling.

    2.	The code submission must be done by sending through a git repository with at least two commits:
        a.	Showing the basic framework and library installation with no modifications
        b.	Showing the application being built and committed without any basic framework setup, this may be done on several commits if desired showing clear progression in building the app.

    3.	Please do not use pre-existing API wrapping libraries (like alaouy/youtube) or submit a pure javascript based implementation where the request to the API is not done in PHP
        a.	We want to see how you interact with an API directly using PHP
        b.	We want to see how you handle the request and the response
        c.	There are many many ways to complete this challenge, but the more you can show off your PHP skills the better.

---------------------------------------------------------------------------------------------------------------
