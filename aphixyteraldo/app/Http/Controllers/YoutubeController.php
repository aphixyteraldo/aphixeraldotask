<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;

class YoutubeController extends Controller
{

    public function __construct(){
        $this->ytKey = "AIzaSyCshlchUO4g8oz1XKfXDhqzFs9cOWVUX7s";
        $this->returnArr = [];
    }

    public function index(){
        return view('youtube');
    }

    /*get ajax search from front-end and return the results*/
    public function functionsYt(){

        $options = $_POST['option'];

        //if you want to make more functions, only add more options in case
        switch ($options) {
            case "search":

                //using Guzzle native from laravel to make http requests
                $client = new Client();
                $query = $_POST['content'];
                $params = [
                    'query' => [
                       'key' => $this->ytKey,
                       'q' => $query,
                       'part' => 'snippet',
                       'maxResults' => 25
                    ]
                ];
                $response = $client->request('GET','https://www.googleapis.com/youtube/v3/search',$params);
                $decoded = json_decode($response->getBody(), JSON_OBJECT_AS_ARRAY);
            
                //if there's result
                if($decoded['pageInfo']['totalResults'] > 0){
                    foreach ($decoded['items'] as $searchResult) {
                        switch ($searchResult['id']['kind']) {
                            case 'youtube#video':
                                $videos[] = [
                                    'channelTitle' => $searchResult['snippet']['channelTitle'],
                                    'videosTitle' => $searchResult['snippet']['title'],
                                    'videosLinks' => 'http://youtu.be/'.$searchResult['id']['videoId'],
                                    'date' => $searchResult['snippet']['publishTime'],
                                    'photo' => $searchResult['snippet']['thumbnails']['high']['url']
                                ];
                                break;
                            //if you want to find channels        
                            case 'youtube#channel':
                                $channels[] = [
                                    'channelsTitle' => $searchResult['snippet']['title'],
                                    'channelsId' => $searchResult['id']['channelId'],
                                    'date' => $searchResult['snippet']['publishTime'],
                                    'photo' => $searchResult['snippet']['thumbnails']['high']['url']
                                ];
                                break;
                            //if you want to find playlists     
                            case 'youtube#playlist':
                                $playlists[] = [
                                    'playlistsTittle' =>  $searchResult['snippet']['title'],
                                    'playlistsId' => $searchResult['id']['playlistId'],
                                    'date' => $searchResult['snippet']['publishTime'],
                                    'photo' => $searchResult['snippet']['thumbnails']['high']['url']
                                ];
                                break;
                        }
                    }
                    $this->returnArr['returnCode'] = 1;
                    $this->returnArr['returnTotalFound'] = $decoded['pageInfo']['totalResults'];
                    if(isset($videos)){
                        $this->returnArr['videos'] = $videos;
                    }
                    if(isset($channels)){
                        $this->returnArr['channels'] = $channels;
                    }
                    if(isset($playlists)){
                        $this->returnArr['playlists'] = $playlists;
                    }

                    return json_encode($this->returnArr);
                }else{
                    $this->returnArr['returnCode'] = 0;
                    return json_encode($this->returnArr);
                }                
            break;
        }
        
    }
}
